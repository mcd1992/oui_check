#include <string.h>
#include "ishex.h"

int isHex(char *string){
	char hexChars[] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','A','B','C','D','E','F',':'};
	int hexCharLen = strlen(hexChars);
	int stringLen = strlen(string);
	int strIsHex = 0;
	int sChar;
	int hChar;

	for (sChar = 0; sChar < stringLen; sChar++){
		strIsHex = 0;
		for (hChar = 0; hChar < hexCharLen; hChar++){
			if ( string[sChar] == hexChars[hChar] ){
				strIsHex = 1;
				break;
			}
		}
		if ( strIsHex == 0 ) { return strIsHex; }
	}
	return strIsHex;
}
