oui_check
=========

Quickly get the manufacturer of a given mac address.

```
OUI Checker by Aaron M.
Usage: ouicheck [options] <MAC ADDR> [MAC 2] [MAC 3]
Options:
	-d	Debugging Output
	-s	Manuf Search (Reverse Search for Manuf -> OUI)
	-m	Print Max 10 Char Manuf Only (Clean Output)
STDIN Pipe is Supported (cat macfile | ouicheck)
```


This was my first C program written that wasn't a helloworld. Contrary to the help saying it supports pipes it doesn't look the code actually does.
The basics of this command is `ouicheck A1:B2:C3:D4:E5:F6`, it will return the line out of the wiresharks manuf file that matches.

