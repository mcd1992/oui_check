CC		=	gcc
CFLAGS		=	-c -g -Wall
LDFLAGS		=
SOURCES		=	main.c ishex.c
OBJECTS		=	$(SOURCES:.c=.o)
EXECUTABLE	=	ouicheck

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

..o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o $(EXECUTABLE)
