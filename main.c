#define _GNU_SOURCE //strcasestr needs this

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "ishex.h"

//#define OUIFile "/usr/local/share/wireshark/manuf"
#define OUIFile "/usr/share/wireshark/manuf"

FILE *ouiFile;
char *macAddr;
char *ouiOut;
char *manufOut;
char *cleanOuiOut;
char *fileBuf;
unsigned long ouiFSize;

//static int debug = 1; //force debug without the -d flag
int debug = 0;
int scanManuf = 0;
int manufOnly = 0;
int stdinPipe = 0;




void findManufFile(){
	if ( access("./manuf", F_OK) != -1){
		if (debug == 1) {printf("USING ./manuf\n");}
		ouiFile = fopen("./manuf","r");
	}
	else if ( access(OUIFile, F_OK) != -1){
		if (debug == 1) {printf("USING OUIFile\n");}
		ouiFile = fopen(OUIFile,"r");
	}
	else {
		printf("Manuf File Not Found\n");
		exit(1);
	}
}




void usage(char *fileName){
	printf("\nOUI Checker by Aaron M.\n"\
	"Usage: %s [options] <MAC ADDR> [MAC 2] [MAC 3]\n"\
	"Options:\n"\
		"\t-d\tDebugging Output\n"\
		"\t-s\tManuf Search (Reverse Search for Manuf -> OUI)\n"\
		"\t-m\tPrint Max 10 Char Manuf Only (Clean Output)\n"\
	"STDIN Pipe is Supported (cat macfile | %s)\n\n"
	,fileName,fileName);
}




void findOui(char *macTry, int cleanOutput){
	strncpy(macAddr, macTry, 8);//cp just the oui FIXME:support if mac has no :'s

	char *match = strcasestr(fileBuf,macAddr);
	if ( match != NULL ){
		char *end = strpbrk(match, "\n");
		ouiOut = malloc(128);
		memset(ouiOut, 0, 128);
		strncpy(ouiOut, match, end - match);
		if (cleanOutput == 0){
			printf("%s\n",ouiOut);
		}
		else if (cleanOutput == 1){
			char *cleanOui = strpbrk(ouiOut, "\t");
			cleanOui++;//move over one byte, remove \t
			cleanOuiOut = malloc(10);
			strncpy(cleanOuiOut, cleanOui, 10);
			printf("%s\n", cleanOuiOut);
		}
		else {
			printf("Called findOui() without cleanOutput arg\n");
			exit(1);
		}
	}
	else {
		if (debug == 1) {printf("No match found :(\n");}
	}
}


void findManufOuis(char *manufLookup){
	char *manufSearch = strcasestr(fileBuf, manufLookup);
	if ( manufSearch != NULL ) {
		manufSearch-=9; //Bootleg jump back to get the OUI
		char *end = strpbrk(manufSearch, "\n");
		manufOut = malloc(128);
		memset(manufOut, 0, 128);
		strncpy(manufOut, manufSearch, end - manufSearch);
		printf("%s\n", manufOut);
	}
	else {
		if (debug == 1) {printf("No match found\n");}
	}
}


void setupOuidb(){
	fseek(ouiFile, 0, SEEK_END);//get the EOF
	ouiFSize = ftell(ouiFile);//get the file offset / size (EOF)
	rewind(ouiFile);//set the file offset back to start

	fileBuf = malloc(ouiFSize);//set the file pointer (raw memory for file data)
	memset(fileBuf, 0, ouiFSize);//fill the file pointer with null
	fread(fileBuf, 1, ouiFSize, ouiFile);//fill the file pointer with the data
	fclose(ouiFile);//close the oui manuf file

	macAddr = malloc(8);//pointer for oui / macAddr
	memset(macAddr, 0 , 8);//fill the macAddr with nulls
	if (debug == 1) {printf("Manuf File Size: %li\n",ouiFSize);}
}


int cleanExit(){//fill the malloc'd areas with nulls then free it
	if ( fileBuf != NULL) {
		memset(fileBuf, 0, ouiFSize);
		free(fileBuf);
	}
	if ( macAddr != NULL ) {
		memset(macAddr, 0, 8);
		free(macAddr);
	}
	if ( ouiOut != NULL ) {
		memset(ouiOut, 0, 128);
		free(ouiOut);
	}
	if ( cleanOuiOut != NULL ) {
		memset(cleanOuiOut, 0, 10);
		free(cleanOuiOut);
	}
	if ( manufOut != NULL ){
		memset(manufOut, 0, 128);
		free(manufOut);
	}
	return 0;
}


int main(int argc, char *argv[]){
	int i = 1;
	if (argc == 1){//Calling with no arguments
		usage(argv[0]);
		exit(1);
	}
	findManufFile();
	setupOuidb();

	while (i < argc){//iterate through argv and set the flags
		if (strcmp(argv[i], "-d") == 0){// would switch() be more effecient?
			debug = 1;
			if (debug == 1) {printf("debugging output enabled\n");}
		}
		else if (strcmp(argv[i], "-s") == 0){
			scanManuf = 1;
			if (debug == 1) {printf("Reverse Scanning for Manuf\n");}
		}
		else if (strcmp(argv[i], "-m") == 0){
			manufOnly = 1;
			if (debug == 1) {printf("Printing manuf only (clean output)\n");}
		}
		else {
			if ( !isHex(argv[i]) || strlen(argv[i]) <= 2 ){
				if (debug == 1) {printf("Not a valid Argument, MAC, Manuf\n");}
				usage(argv[0]);
				exit(1);
			}
			break; // flags are set & next arg is a MAC or Manuf
		}
		i++;
	}

	if (i >= argc){//supplied flags but no mac[s]
		if (debug == 1) {printf("\n\nNo MAC Address To Check\n");}
		usage(argv[0]);
		exit(1);
	}

	while (i < argc){ //continue where flag search left off
		if (manufOnly == 0 && scanManuf == 0){//default full line output
			if ( isHex(argv[i]) == 1 ) {
				findOui(argv[i],0);
			}else{ printf("Invalid MAC (Not HEX)\n"); exit(1); }
		}
		else if (manufOnly == 1 && scanManuf == 0){//output 10 digit OUI
			if ( isHex(argv[i]) == 1 ) {
				findOui(argv[i],1);
			}else{ printf("Invalid MAC (Not HEX)\n"); exit(1); }
		}
		else if (manufOnly == 0 && scanManuf == 1){//reverse lookup
			if ( isHex(argv[i]) == 0 ){
				findManufOuis(argv[i]);
			}else{ printf("You specified reverse lookup but gave a MAC\n"); exit(1); }
		}
		else {
			printf("Invalid Argument[s]\n");
			exit(1);
		}
		i++;
	}

	return cleanExit();
}
//FIXME: search mac without colons, reverse (manuf -> oui) search, search from stdin pipe, download manuf file if not found
